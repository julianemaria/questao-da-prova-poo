public class Book extends Item{
  private String author;
  private int edition; 
  private int volume;

  public Book(String author, int edition, int volume, String title, String publisher, int yearPublished, int isbn, double price){
    super(title,  publisher,  yearPublished,  isbn,  price);
    this.author = author;
    this.edition = edition;
    this.volume = volume;
  }

  public void display(){
      super.display();
      System.out.println("ISBN: " + this.isbn);
      System.out.println("Preço: " + this.price);
      System.out.println("Autor: "+ this.author);
      System.out.println("Edição: "+ this.edition);
      System.out.println("Volume: "+ this.volume);
  }

 
}
