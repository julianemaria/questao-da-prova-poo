public class Item{

  protected String title;
  protected String publisher;
  protected int yearPublished;
  protected int isbn;
  protected double price; 

  public Item (String title, String publisher, int yearPublished, int isbn, double price){
    this.title = title;
    this.publisher = publisher;
    this.yearPublished = yearPublished;
    this.isbn = isbn;
    this.price = price; 
  }

  public void display(){
      System.out.println("Titulo: "+ this.title);
      System.out.println("Editora: "+ this.publisher);
      System.out.println("Ano de publicação: " + this.yearPublished);
      System.out.println("ISBN: " + this.isbn);
      System.out.println("Preço: " + this.price);
  }
}
